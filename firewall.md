# FIREWALL

![Firewall](https://www.pandasecurity.com/mediacenter/src/uploads/2019/01/pandasecurity-what-it-firewall.jpg)

As a name, it illustrates that a **Firewall** is a barrier that protects the building from the fire to spreads out all over the Building.  
In technical terms, A **Firewall** is a software or the hardware that protects our system from the anonymous connections from the internet.  

## UTILIZATION OF FIREWALLS
* Firewall helps us to exhibit **hackers**, **viruses** and **worms** that try to penetrates through the network into our system.  

* A firewall is essential in large organizations like the banking sector or other authorized sectors for highly recommended to the security purposes.  

* A firewall provides the personal computer to protect from the harmful attack.  

## PROCESS OF PERMISSION GRANTED AND DENIED
A firewall will controls the access by considering the following key aspects.  
* Ports
* Protocols
* Domain names
* Programs
* Key words

A firewall follows up the specific rules that are customized or implemented by the network administrator.  
For example, consider the following rules specified by the network administrator.  
Permission - IP address - Protocol - Destination - port  
ALLOW - ANY - TCP - ANY - 80    
DENIED - ANY - UDP - ANY - 23  

The firewall works by filtering the data from the incoming connections and look out the rules that are provided by the network administrator. If the rules are satisfied, the firewall allows the respective connections into the node.

## MAJOR TYPES OF FIREWALL
There are two major types of firewalls divided by what they prevent.
* Host based firewall
* Network based firewall

### HOST BASED FIREWALL
![Host based firewall](https://cadictechnologies.com/wp-content/uploads/2018/04/Computer-Firewall.png)  
In this type, the firewall relies on the installed firewall software. It protects the computer itself from the malpractice actions over the network into this computer.  
Microsoft Operating system has it's own package of host based firewall.
The firewall available through [third party](https://www.techradar.com/in/best/firewall) software installations.

### NETWORK BASED FIREWALL
![network based firewall](https://www.vrstech.com/blog/wp-content/uploads/2019/07/firewall-network-security-system-dubai.jpg)  
Network based firewall includes both software and hardware. It operates on the network layer.
Each computer in the network have their own firewall protection. The network based firewall does not know about the applications and vulnerabilities on a machine. The network based firewall protecting the whole network, it does not provide any help if the malicious attack is originating from inside the network itself.  
The network based firewall does not know about the applications and vulnerabilities on a machine. The network based firewall protecting the whole network, it does not provide any help if the malicious attack is originating from inside the network itself.  
Some of the popular network based firewalls,  
* Barracuda NextGen Firewall
* Cisco Firepower Series
* Fortinet FortiGate
* Juniper SRX
* Meraki MX Series

## GENERATIONS OF FIREWALL
1. Packet Filtering Firewall :  
In the packet filtering firewall, each packet is compared to a set of criteria before it is forwarded. Most routers support packet filtering.  
2. Stateful Inspection Firewall :  
It figures out the state of a connection for the packet and tracks the network connections that moving from the source to the destination over the internet.
3. Application Layer Firewall :  
The Application level firewall filters the packet at the application layer of the OSI model. Incoming or outgoing packets cannot access service when there is no proxy. It provides the highest level of security.  
4. Next Generation Firewall :  
The next generation of the firewall inspects the packet deeper than the other. It provides a better detection capability for any suspicious activity.  

## REFERENCE LINK
[Firewall Types](https://www.electrical4u.net/network/what-is-firewall-types-of-firewall-full-explanation/)  
[Firewall in Network Security](https://medium.com/@neil.wilston123/firewall-in-network-security-2a98795fcac1)  